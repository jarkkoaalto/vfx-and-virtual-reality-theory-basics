# VFX and Virtual Reality Theory Basics by Vicki Lau #

## Section 01 - VR Defined ##

What exactly is Virtual Reality?

Google say "VR" and see what you find

- Comupter-generated simulation
- 3D image or environment
- Fully-inversive digital application
- Interactive or non-interactive
- Using special hardware e.g. helmetm deedback devices
- it's a digital immersive reality.


### What is Virtual Reality, according to Gamers? ###

Platform of exploration, immersion and entertaiment. 

- 3D artificial fully-immersive environment
- Most people's opition of what VR is
- A platform for gamers by gamers

### What is Virtual Reality, accorting to the military & Government organizations? ###

Training method for military division and strategic teams.

- Training for re-reacted compat situation
- Training fo reflexes and actions in high-intemsity scenarious
- Skill enhabcement and real-time analysis

### What is Virtual Reality, according to adventerisers? ###

Platform to access new or different types of customers

- Delivery of VR in-experience ads
- Part of a larger brand marketing of awarness campaing
- A unique advertising tool

### What is Virtual Reality, according to Healthcare Pros? ###

Innovative way of training and also healing and rehabilitating patients

- Multi-faceted tool forr practitioners, apperentices and patients
- Means of empowering and helping patients
- Skills training in simulated environments

### What is Virtual Reality, according to Real Wstate pros? ###

Effective way to showcase projects to local and global prospects

- Remotely visit sites, homes, cities and even countries
- Visualize the ideal hone with virtual interior design
- Useful tool in the comstruction process

### What is Virtual Reality, according this Course ###

Computer-generated simulation of a fully-immrsive digital environment

- Computer-genereated simulation
- 3D image or environment
- Fully-inversive digital application
- Interactive or non-interactive
- Using special hardware. e.g. helmet, feedback devices


### VR Basic Terms ###

- Virtual Reality (VR)

VR places the user in anothher location entirely. Whether that location is computer generated or captured by video, it enitely occludes the user's nature surrounding

- Augmented reality (AR)

In augmented reality - like Google Glass or the Yelp app's Monocle feature on the mobile devices - the visible natural world is overlaid with a layer of digital content.

- Mixed Reality (MR)

In technologies like Magic Leap's virtual objects are integrated into - and responsive to - the natural world. A virtual ball under you desk. For example, would be blocked from vlew unless you bend down look at it. In Theory. MR could become VRin a dark room.

- extended Reality (XR)

is combination all abowe

- Headset / helmetm

Device worm to engage in XR experience or external gear (not necessarily worn) used to experience VR CAVE: cave automatic virtual environment

- Heads-Up Display (HUD)

Display overlaying your view, presenting data and visual like (IRON MAN movie)

- HMD

Head-mounded device

- 3D / Full 3D

Anything that has or conveys dimensional depth to viewers

- Immersive

Experience that appears to fully involve a user's senses

- 360 / 360 VR.

Live-action virtual reality

- Cinematic VR.

- Panoramic video

Also ... live-action virtual reality (but it could be a still or video)

- Heptics

Device that aid in manipulating the sense of virtual or real touch

- Dolby atmos / Surround sound

Used in theaters to re-create 3D sound in enclose environments

- Spatial audio

Audio enhancement that reproduces sound depth in an environment

- Interactive VR / Film

A "choose your own adventure" VR film experience

- Oculus

A virtual reality brand and company

- Vive

Another virtual reality brand and company

### VR Terms and Vocabulary ###

 - Latlong 

Refers to an unwrapped sphere onto a planar map. Positive and negative y-, x- and z-axis

- Equirectangular

Another term for "latlong"

- Fishlens

Wide-angle lens or a projection technique for images

- Little planet

Projection technique also known as stereographic projection

- VR Compositing

Compositing with 360-degree virtual reality footage

- Stitcing

Combining footage from linked cameras to form one panorama

- Rough stitch

Quick first pass panoramic stich

- Fine stitch

Polished and presentable version of a panoramic stitch

- Clean plate 

Footage wirh no actors or moving elements in foreground

- Gaze point 

Circular dot in center of user's point of view in virtual reality

- FOV

Field of view of the user in a  virtual reality experience. Human sterescopic field of view

- Parralax

Relative movement of objects due to changes in point of view

- Nodal point

Point where the lens swings or pans about from 

- Experiential design

Designing for the quality of the user experience

- User experience design (UX / UXD)

UX: Enhancing user satisfaction with an interactive experience

- Valuable
- Accessible
- Desirable
- Credible
- Useful
- Usable
- Findable


UX:

1. Prototype: Functionality
2. Business analysis: Business case
3. Visual Designing: Look and Feel

- Performance
- Interaction
- Interface

Outcome is: Market communication, Information design and Business plan

- Gamifigation

Addition of game-play elements to encourage user engagement

- Frame frate

Frequency of frames displayed in a sequence

- Buzzing / flickering / juddering / stuttering

Visual artefacts formed due to issues in a virtual reality experience

- Latency

Delay between action and reaction

- Dome protection

Immersive dome-based, heatset-less virtual experience

- Projection techniques / mapping

Projection technologies or algorithms that project images onto digital or real objects

- Camera projetion

Projection of images controlled by a virtual camera's point of view

- Stereoscopic VR

Virtual reality experience with 3D depth

- Monoscopic VR

Virtual reality experiences without 3D depth

- Zenith

A point directly above the user in a 360-degree sphere

- Nadir

A point derectly below the user in a 360-degree sphere

# VR in the VFX World #
 
## VR types and comparison ##

- Headset VR
- Room-scale VR
- Retinal Projection VR
- Active VR
- Passive VR
- Interactive VR

The diverse potential of VR aand AR applications

Predictied market size of VR/AP software for diffrenet use cases in 2025

- Enterprise and public sector $16.1 billion
- Consumer $18.9 billion
- Videogames $11.6 billion
- Healtcare $4.7 billion
- Live events $4.1 billion
- Video entertainment $3.2 billion
- Engineering $2.6 billion
- Real estitate $1.6 billion
- Military $1.4 billion
- Education $0.7 biliion

We will focus on non-entertainment VR use cases for now

- VR and marketing. How new furnitures fit room and how to du DIY fixes and repairs
- VR and turism: get a taste outside their comfort zone or Affordable virtual location
- VR and military: train soldiers and replicate immersive intense experiences
- VR and Healthcare: train medical professionals and enhance practices or form of therapeutic tool
- VR and real estate: visit sites and properties remotely and interior design and decor in virtual space
- VR and Supermarket in virtual reality: by or choore different product

## VR Software and tools ##

- Game engines. Interactive experiences are naturally created from game engines. Not restricted to being used fro games alone (unity, Unreal an impropable)
- Cara VR 
- Mettle Mantra VR
- Kolor Av avp, app apg: 360 videos
- PT Gui photo stitching software
- Video stitch studio 
- Worldviz b2b andenterprise (python)
- Virtalis VR4CAD


## VR Hardware and production ##

- Different VR headsets
- Google Jump
- Richo theta
- Gopro
- Jaunt one
- Lytro by google
- Matterpoint 

## VR Work and Projects in VFX ##

Tpyes of VR projects in the film industry

Expectations working on VR projects

Virtual Reality experiene: Designing for the user in 360-degree environments.

Gone of Focus

- Front: Rimary action
- Left: Secondary / Guiding Action
- Right: Secondary / Guiding Action
- Back: Tertiary Action

Directing a user's attention in virtual reality.

Designing for interactivity and action points in virtual reality.

- Working company: like: Jaunt or Within cretes VR short films
- VR animated shorts: Storystudio
- Fully interactive storys: the martians VR experience and being Henry

## VR Prominent Names and Companies ##

- Storystudio $2 billion: henry - cartoon
- Jaunt - live content 
- Within - live content
- Felix and Paul studio: indie content
- Survios - Game studio 
- Owlcgemy Labs Google - Vacation simulation
- AltspaceVR by Microsoft - Social games
- The Void: Mixed reality 

## VR in the VFX World ##

How exactly does VR tie in with VFX? (visual effects)

Acgieving visual perfection on screen.

UP:

- Quality
- Speed 
- Result 

DOWN: 

- Cost

Cimematic VR and how fix movie errors. High frame rates are the default.


# VR and VFX Case Studies #

Within 

- Like any typical VFX studio
- Hired VFX artist for live-action VR
- Engineers add the VR interactivity

Seyenapse

- Seyenapse combines VR with VFX
- Similar workflow process
- Adapting VFX knowledge for VR
- Collaborating like a VFX studio

Oculus Storystudio

- Oculus Storystudio combines VR with VFX:
- Focused in telling the story
- Technial execution play a supporting role
- Establishing a sense of presence in VR